using System;
using System.Reflection;
using UnityEditor;

namespace NaughtyAttributes.Editor
{
    [PropertyValidator(typeof(ValidateInputAttribute))]
    public class ValidateInputPropertyValidator : PropertyValidator
    {
        public override void ValidateProperty(SerializedProperty property)
        {
            ValidateInputAttribute validateInputAttribute = PropertyUtility.GetAttribute<ValidateInputAttribute>(property);
            UnityEngine.Object target = PropertyUtility.GetTargetObject(property);

            MethodInfo validationCallback = ReflectionUtility.GetMethod(target, validateInputAttribute.CallbackName);

            if (validationCallback != null &&
                (validationCallback.ReturnType == typeof(bool) || validationCallback.ReturnType == typeof(string)) &&
                validationCallback.GetParameters().Length == 1)
            {
                FieldInfo fieldInfo = ReflectionUtility.GetField(target, property.name);
                Type fieldType = fieldInfo.FieldType;
                Type parameterType = validationCallback.GetParameters()[0].ParameterType;

                if (fieldType == parameterType) {
                    var result = validationCallback.Invoke(target, new object[] { fieldInfo.GetValue(target) });
                    bool success;
                    var message = string.IsNullOrEmpty(validateInputAttribute.Message) ? property.name + " is not valid" : validateInputAttribute.Message;
                    switch (validationCallback.ReturnType.Name)
                    {
                        case "Boolean":
                            success = (bool) result;
                            break;

                        case "String":
                            message = (string) result;
                            success = string.IsNullOrEmpty(message);
                            break;

                        default:
                            UnityEngine.Debug.LogErrorFormat("type:{0} name:\"{1}\"", validationCallback.ReturnType, validationCallback.ReturnType.Name);
                            success = false; // can't actually get here due to type test above
                            break;
                    }

                    if (!success)
                    {
                        EditorDrawUtility.DrawHelpBox(message, MessageType.Error, context: target);
                    }
                }
                else
                {
                    string warning = "The field type is not the same as the callback's parameter type";
                    EditorDrawUtility.DrawHelpBox(warning, MessageType.Warning, context: target);
                }
            }
            else
            {
                string warning =
                    validateInputAttribute.GetType().Name +
                    " needs a callback with boolean return type and a single parameter of the same type as the field";

                EditorDrawUtility.DrawHelpBox(warning, MessageType.Warning, context: target);
            }
        }
    }
}
