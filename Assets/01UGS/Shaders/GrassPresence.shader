// Used as replacement shader for snow and grass.
Shader "Unlit/GrassPresence"
{
    Properties
    {
        // Properties copied from Grass shader for convenience -- not actually used.
        _ColorMapOrig ("ColorMap with grass height from orig grass material, set by SetGlobalTexture", 2D) = "white" {}
    }

    SubShader
    {
        Tags {
            "GrassOrSnow"="Grass"
            "ForceNoShadowCasting"="True"
            "IgnoreProjector"="True"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _ColorMapOrig;
            float4 _ColorMapOrig_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _ColorMapOrig);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_ColorMapOrig, i.uv);
                return col;
//                return fixed4(1,1,1,1); // white -- will want white with alpha 1
            }
            ENDCG
        }
    }

    SubShader
    {
        Tags {
            "GrassOrSnow"="Snow"
            "ForceNoShadowCasting"="True"
            "IgnoreProjector"="True"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return fixed4(0,1,1,0); // cyan + alpha 0 (no grass height)
            }
            ENDCG
        }
    }

}
