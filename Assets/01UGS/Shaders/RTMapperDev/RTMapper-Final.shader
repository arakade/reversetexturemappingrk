﻿Shader "Unlit/RTMapper/Dev/Final-trans"
{
    Properties
    {
        // receives the input RT
        _MainTex ("RT (auto-set by Blit)", 2D) = "white" {}
        // reverse map encoding actual normalized positions to original UV
        _ReverseMap ("Reverse map (set from code)", 2D) = "white" {}
        // output goes to 'screen' (which should be set to the output RT)
    }
    SubShader
    {
        Tags {
			// TODO: "RenderType"="Opaque"
            "RenderType"="Transparent"
			"PreviewType"="Plane"
		}

		// TODO: remove these with transparency
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                // float2 uv : TEXCOORD0;
                float2 uvReverse : TEXCOORD0; // switch to TEXCOORD1 if re-enable uv
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _ReverseMap;

            // These unneeded since not using TRANSFORM_TEX()
            // float4 _MainTex_ST; // gets tiling and offset - used in TRANSFORM_TEX()
            // float4 _ReverseMap_ST; // gets tiling and offset - used in TRANSFORM_TEX()

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                // o.uv = TRANSFORM_TEX(v.uv, _MainTex); // this simply multiplies tiling and offset
                // o.uvReverse = TRANSFORM_TEX(v.uv, _ReverseMap);
                o.uvReverse = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                uint4 mapping = tex2D(_ReverseMap, i.uvReverse) * 255;

                // Decode mapping coordinates from texture value
                // Duplicates code from FixedColor32Helper
			    float2 uvRemapped = float2(mapping.r << 8 | mapping.g, mapping.b << 8 | mapping.a) / 65535;
                // fixed4 col = fixed4(uvRemapped, 0, 1);

			    // Lookup colour from RT for remapped coordinate
                fixed4 col = tex2D(_MainTex, uvRemapped);

                return col;
            }
            ENDCG
        }
    }
}
