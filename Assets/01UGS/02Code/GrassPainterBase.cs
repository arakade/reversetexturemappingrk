#if DIRECTX11_GRASS

using System;
using StixGames.GrassShader;
using UnityEngine;

namespace UGS.grass
{
	[Serializable]
	public abstract class GrassPainterBase : ScriptableObject
	{
		protected const float Epsilon = 0.01f;

		public static readonly int ColorMapId = Shader.PropertyToID("_ColorMap");
		public static readonly int DensityId = Shader.PropertyToID("_Density");

		public TextureTarget Target = TextureTarget.Density;
		public DensityChannel DensityChannel = DensityChannel.R;
		public ColorHeightChannel ColorHeightChannel = ColorHeightChannel.Color;
		public BrushMode Brush = BrushMode.Blend;
		public Color PaintColor = Color.white;
		public Color32 PaintColor32 = Color.white;
		public float PaintDensity = 1;
		public float Strength = 1;
		public float Size = 1;
		public float Softness = 0.5f;
		public float Spacing = 0.5f;
		public float Rotation = 0;

		/// <summary>
		/// The grass collider the brush will be applied to. Only set this when using ApplyBrush directly.
		/// </summary>
		public Collider[] GrassColliders = new Collider[0];

		/// <summary>
		/// The color height texture the brush will be applied to. Only set this when using ApplyBrush directly.
		/// </summary>
		public Texture2D ColorHeightTexture;

		/// <summary>
		/// The density texture the brush will be applied to. Only set this when using ApplyBrush directly.
		/// </summary>
		public Texture2D DensityTexture;

		public Texture2D CurrentTexture
		{
			get
			{
				switch (Target)
				{
					case TextureTarget.ColorHeight:
						return ColorHeightTexture;
					case TextureTarget.Density:
						return DensityTexture;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}

			set
			{
				switch (Target)
				{
					case TextureTarget.ColorHeight:
						ColorHeightTexture = value;
						break;
					case TextureTarget.Density:
						DensityTexture = value;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public abstract void init();

		public bool Draw(Ray ray)
		{
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit))
			{
				GrassColliders = new [] { hit.collider };
				ColorHeightTexture = hit.collider.GetComponent<Renderer>().material.GetTexture(ColorMapId) as Texture2D;
				DensityTexture = hit.collider.GetComponent<Renderer>().material.GetTexture(DensityId) as Texture2D;

				ApplyBrush(hit.point);
				return true;
			}

			return false;
		}

		public abstract void ApplyBrush(Vector3 hitPoint);

		public bool RaycastColliders(Ray ray, out RaycastHit hit)
		{
			hit = new RaycastHit();
			foreach (var collider in GrassColliders)
			{
				if (collider.Raycast(ray, out hit, Mathf.Infinity))
				{
					return true;
				}
			}
			return false;
		}
	}

	public enum TextureTarget
	{
		ColorHeight,
		Density
	}

	public enum BrushMode
	{
		Blend,
		Add,
		Subtract
	}

	public enum ColorHeightChannel
	{
		Color,
		Height,
		Both
	}

	public enum DensityChannel
	{
		R,G,B,A
	}
}

#endif // DIRECTX11_GRASS

