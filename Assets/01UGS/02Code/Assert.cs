using JetBrains.Annotations;

namespace UGS.unityutils {
	public static class Assert {
		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		[StringFormatMethod("msgFmt")]
		public static void assertIsTrue(this UnityEngine.Object context, bool condition, object msgFmt = null, params object[] args) {
			if (condition)
				return;

			if (null == msgFmt) {
				msgFmt = "False (should be true)";
			}
			fail(context, msgFmt, args);
		}

		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void assertIsTrue<T>(this UnityEngine.Object context, bool condition, T value) where T : struct {
			if (condition)
				return;

			fail(context, value);
		}

		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		[StringFormatMethod("msgFmt")]
		public static void assertIsFalse(this UnityEngine.Object context, bool condition, object msgFmt = null, params object[] args) {
			if (!condition)
				return;

			if (null == msgFmt) {
				msgFmt = "True (should be false)";
			}
			fail(context, msgFmt, args);
		}

		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void assertIsFalse<T>(this UnityEngine.Object context, bool condition, T value) where T : struct {
			if (!condition)
				return;

			fail(context, value);
		}

		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		[StringFormatMethod("msgFmt")]
		public static void assertIsNull(this UnityEngine.Object context, object reference, object msgFmt = null, params object[] args) {
			if (null == reference)
				return;

			if (null == msgFmt) {
				msgFmt = "Not null (should be) : {0}";
				args = new []{ reference };
			}
            fail(context, msgFmt, args);
		}

		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void assertIsNull<T>(this UnityEngine.Object context, object reference, T value) where T : struct {
			if (null == reference)
				return;

            fail(context, value);
		}

		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		[StringFormatMethod("msgFmt")]
		public static void assertIsNotNull(this UnityEngine.Object context, object reference, object msgFmt = null, params object[] args) {
			if (null != reference)
				return;

			if (null == msgFmt) {
				msgFmt = "Null (should not be)";
			}
			fail(context, msgFmt, args);
		}

		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void assertIsNotNull<T>(this UnityEngine.Object context, object reference, T value) where T : struct {
			if (null != reference)
				return;

			fail(context, value);
		}

		[StringFormatMethod("msgFmt")]
		private static void fail(UnityEngine.Object context, object msgFmt, object[] args) {
			switch (msgFmt) {
				case null:
					UnityEngine.Debug.LogError("Assertion failed", context);
					break;
				case string fmt:
					UnityEngine.Debug.LogErrorFormat(context, fmt, args);
					break;
				default:
					UnityEngine.Debug.LogErrorFormat(context, msgFmt.ToString());
					break;
			}
			UnityEngine.Debug.Break();
		}

		private static void fail<T>(UnityEngine.Object context, T value) where T : struct {
			UnityEngine.Debug.LogErrorFormat(context, value.ToString());
			UnityEngine.Debug.Break();
		}
	}
}
