#if DIRECTX11_GRASS

using UnityEngine;
using UnityEngine.Assertions;

namespace UGS.grass {
	public sealed class ExerciseGrassPainter : UnityEngine.MonoBehaviour {
#region serialized

		[SerializeField]
		private GrassPainterBase grassPainter = null;

		[SerializeField]
		private int loops = 100;

		[SerializeField]
		private Vector3 point = Vector3.zero;

#endregion serialized
#region Unity callbacks

		private void OnEnable() {
			setup();
		}

		public void Update() {
			grassPainter.ApplyBrush(point);

			if (--loops <= 0)
				enabled = false;
		}

		public void OnDisable() {
			if (null != textureToDestroy) {
				Destroy(textureToDestroy);
				textureToDestroy = null;
			}

			if (null != materialToDestroy) {
				Destroy(materialToDestroy);
				materialToDestroy = null;
			}

			// reset to using sharedMaterial?
			var rend = GetComponent<Renderer>(); // material creates instance whereas sharedMaterial affects version in Editor!
			rend.material = null;
			rend.sharedMaterial = materialToRestore;

			grassPainter.ColorHeightTexture = null;
			grassPainter.GrassColliders = null;
		}

#endregion Unity callbacks
#region public

#endregion public
#region internal

#endregion internal
#region private

		private void setup() {
			var rend = GetComponent<Renderer>();
			materialToRestore = rend.sharedMaterial;
			materialToDestroy = rend.material; // material creates instance whereas sharedMaterial affects version in Editor!
			var id = Shader.PropertyToID("_MainTex");
			// var textureToClone = materialToDestroy.GetTexture(GrassPainterBase.ColorMapId) as Texture2D;
			var textureToClone = materialToDestroy.GetTexture(id) as Texture2D;
			Assert.IsNotNull(textureToClone, "Null texture from " + this);
			textureToDestroy = new Texture2D(textureToClone.width, textureToClone.height, textureToClone.format, true);
			Graphics.CopyTexture(textureToClone, textureToDestroy);
			// materialToDestroy.SetTexture(GrassPainterBase.ColorMapId, textureToDestroy);
			materialToDestroy.SetTexture(id, textureToDestroy);

			// assign to GrassPainter
			grassPainter.ColorHeightTexture = textureToDestroy;
			grassPainter.GrassColliders = new [] { GetComponent<Collider>() };

			grassPainter.init();
		}

		private Material materialToDestroy;

		private Texture2D textureToDestroy;

		private Material materialToRestore;

#endregion private
	}
}

#endif // DIRECTX11_GRASS
