﻿using System;
using NUnit.Framework;
using UnityEngine;
using static UGS.grass.FixedColor32Helper;

namespace UGS.unityutils.tests {
	public class TestFixedColor32Helper {
		private const float TOLERANCE = 0.00001f;

		[Test]
		public void confirmMax() {
			assertInt(Max, 65535);
		}

		[Test]
		public void roundTrip_00() {
			float x0 = 0f, y0 = 0f;
			convert(convert(x0, y0), out var x1, out var y1);
			assertFloatApprox(x0, x1);
			assertFloatApprox(y0, y1);
		}

		[Test]
		public void roundTrip_11() {
			float x0 = 1f, y0 = 1f;
			convert(convert(x0, y0), out var x1, out var y1);
			assertFloatApprox(x0, x1);
			assertFloatApprox(y0, y1);
		}

		[Test]
		public void negative() {
			NUnit.Framework.Assert.Throws<ArgumentOutOfRangeException>(() => convert(-1f, 0f));
		}

		[Test]
		public void tooHigh() {
			NUnit.Framework.Assert.Throws<ArgumentOutOfRangeException>(() => convert(1.0001f, 0f));
		}

		[Test]
		public void valuesX(
			[Values(0f, 1f, 0.5f, 0.1f, 0.01f, 0.001f, 0.0001f)] float x0) {

			convert(convert(x0, 0f), out var x1, out var unused);
            assertFloatApprox(x0, x1);
		}

		[Test]
		public void valuesY(
			[Values(0f, 1f, 0.5f, 0.1f, 0.01f, 0.001f, 0.0001f)] float y0) {

			convert(convert(0f, y0), out var unused, out var y1);
            assertFloatApprox(y0, y1);
		}

		[Test, Pairwise]
		public void valuesPairwise(
			[Values(0f, 1f, 0.1f, 0.01f, 0.001f, 0.0001f)] float x0,
			[Values(0f, 1f, 0.1f, 0.01f, 0.001f, 0.0001f)] float y0) {

			convert(convert(x0, y0), out var x1, out var y1);
			assertFloatApprox(x0, x1);
			assertFloatApprox(y0, y1);
		}

		[Test]
		public void convertToColor32_00() {
			Color32 c = convert(0f, 0f);
			assertColor32Codes(c, 0, 0, 0, 0);
		}

		[Test]
		public void convertToColor32_11() {
			Color32 c = convert(1f, 1f);
			assertColor32Codes(c, 255, 255, 255, 255);
		}

		[Test]
		public void convertToColor_00() {
			Color c = convert(0f, 0f);
			assertColorCodes(c, 0, 0, 0, 0);
		}

		[Test]
		public void convertToColor_11() {
			Color c = convert(1f, 1f);
			assertColorCodes(c, 1f, 1f, 1f, 1f);
		}

		[Test]
		public void convertFromColor32_00() {
			convert(new Color32(), out var x, out var y);
			assertFloatApprox(0f, x);
			assertFloatApprox(0f, y);
		}

		[Test]
		public void convertFromColor32_11() {
			convert(new Color32(255, 255, 255, 255), out var x, out var y);
			assertFloatApprox(1f, x);
			assertFloatApprox(1f, y);
		}

		[Test]
		public void convertFromColor_00() {
			convert(new Color(), out var x, out var y);
			assertFloatApprox(0f, x);
			assertFloatApprox(0f, y);
		}

		[Test]
		public void convertFromColor_11() {
			convert(new Color(1f, 1f, 1f, 1f), out var x, out var y);
			assertFloatApprox(1f, x);
			assertFloatApprox(1f, y);
		}

		[Test]
		public void roundTripValues([ValueSource(nameof(pairs))] Pair pair) {
            convert(convert(pair.x, pair.y), out var x1, out var y1);
            assertFloatApprox(pair.x, x1);
            assertFloatApprox(pair.y, y1);
		}

		private static void assertColor32Codes(Color32 c, byte r, byte g, byte b, byte a) {
			assertByte(r, c.r);
			assertByte(g, c.g);
			assertByte(b, c.b);
			assertByte(a, c.a);
		}

		private static void assertColorCodes(Color c, float r, float g, float b, float a) {
			assertFloatApprox(r, c.r);
			assertFloatApprox(g, c.g);
			assertFloatApprox(b, c.b);
			assertFloatApprox(a, c.a);
		}

		private static void assertByte(byte expected, byte actual) {
			NUnit.Framework.Assert.AreEqual(expected, actual, "expected:{0} != actual:{1}", expected, actual);
		}

		private static void assertInt(int expected, int actual) {
			NUnit.Framework.Assert.AreEqual(expected, actual, "expected:{0} != actual:{1}", expected, actual);
		}

		private static void assertFloatApprox(float expected, float actual) {
			NUnit.Framework.Assert.IsTrue(approx(expected, actual), "expected:{0} != actual:{1}", expected, actual);
		}

		private static bool approx(float a, float b) {
			return Mathf.Abs(a - b) < TOLERANCE;
			// return Mathf.Approximately(a, b);
		}

		private static Pair[] pairs = new []{
			new Pair(0f, 0f)
		};

		public sealed class Pair {
			public readonly float x, y;

			public Pair(float x, float y) {
				this.x = x;
				this.y = y;
			}

			public override string ToString() {
				return $"{nameof(x)}:{x}, {nameof(y)}:{y}";
			}
		}
	}
}
