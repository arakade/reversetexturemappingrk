#if UNITY_EDITOR
#define DEBUG_UGS
#endif

using NaughtyAttributes;
using UGS.unityutils;
using UnityEngine;
#if DIRECTX11_GRASS
using StixGames.GrassShader;
#endif

namespace UGS.grass {
	/// <summary>
	/// Update *only a section* of a <see cref="RenderTexture"/> with occluding geometry.
	///
	/// Camera is mapped from world-space through <see cref="outputMeshLocalBounds"/> to a XZ plane that is then mapped to <see cref="rt"/>.
	/// Copies the <see cref="grassTextureOrig"/> to the <see cref="rt"/> at <see cref="init"/> then assigns the RT to the material.
	/// Actually now uses 2 RenderTextures:
	///  (1) one to receive initial image from orthogonal camera with occluding geometry,
	///  (2) second to receive a transformed version of (1).
	///      It is mapped through a shader (via <see cref="mappingMaterialPrefab"/>) + a <see cref="reverseMap"/>.
	///      <see cref="setupReverseMap"/> creates the reverseMap which maps local XZ plane coords to UV used by the mesh.
	///      Previously a simple linear interpolation was used but the orthoCamera/RT didn't account for the UV map in their mapping.
	///
	/// Uses the <see cref="outputMeshRenderer"/>'s bounds in XZ as proportional to the target texture (assumes a planar UV mapping or close to).
	///
	/// In the Editor, has an <see cref="areaGUI"/> and context commands to invoke.
	///
	/// At runtime, can either render a bounds (<see cref="renderArea(UnityEngine.Bounds)"/>) or, more efficiently,
	/// have the size set beforehand and just use a point (<see cref="setAreaForFuture"/> + <see cref="renderPosition"/>).
	/// </summary>
	public sealed class RenderSnowArea : MonoBehaviour {
#region serialized

		[SerializeField]
		[Required]
		private Camera snowCam = null;

		[Tooltip("What renders the grass.  Used for bounds.")]
		[SerializeField]
		[Required]
		private MeshRenderer outputMeshRenderer = null;

		[SerializeField]
		[Required]
		private RenderTexture rt = null; // TODO: Assign at runtime

		[SerializeField]
		[Required]
		private RenderTexture mappedRT = null; // TODO: Assign at runtime

		[Tooltip("Material with shader that maps rt to mappedRT using reverseMap.\n" +
		         "Will have its materials set by code.")]
		[SerializeField]
		[Required]
		private Material mappingMaterialPrefab = null;

		[SerializeField]
		[Required]
		private Texture2D grassTextureOrig = null;

		[SerializeField]
		[Required]
		private Shader replacementShader = null;

		[SerializeField]
		private string replacementTag = "GrassOrSnow";

		[Header("Mapping")] // see setupReverseMap() for details

		[Tooltip("This seems to be required.")]
		[SerializeField]
		private bool rotateReverseMap180 = true;

		[Tooltip("Mapping from mesh-normalized (0..1,0..1) coords to texture coords (0..1,0..1).")]
		[SerializeField]
		[Required]
		private Texture2D reverseMap = null;

#if DEBUG_UGS

		[SerializeField]
		private Bounds areaGUI;

#endif

#endregion serialized
#region Unity callbacks

#if UNITY_EDITOR
		public void OnValidate() {
			// some checks should be done on prefabs *and* instances
			if (null == replacementShader) {
				replacementShader = Shader.Find("Unlit/GrassPresence");
			}
			this.assertIsNotNull(replacementShader, nameof(replacementShader));
			this.assertIsFalse(string.IsNullOrEmpty(replacementTag), "using an empty tag causes everything to use replacementShader");

			// some only on instances
			if (UnityEditor.PrefabUtility.IsPartOfAnyPrefab(this) && !UnityEditor.PrefabUtility.IsPartOfPrefabInstance(this))
				return; // skip these parts for prefab since won't have them set

			this.assertIsNotNull(outputMeshRenderer, nameof(outputMeshRenderer));
			this.assertIsNotNull(snowCam, nameof(snowCam));
			this.assertIsNotNull(rt, nameof(rt));
			this.assertIsNotNull(mappedRT, nameof(mappedRT));
			this.assertIsNotNull(mappingMaterialPrefab, nameof(mappingMaterialPrefab));
			if (null == grassTextureOrig) {
				foreach (var textureIds in TextureIds) {
					var texture = outputMeshRenderer.GetComponent<Renderer>().sharedMaterial.GetTexture(textureIds);
					if (null != texture && texture is Texture2D texture2D) {
						grassTextureOrig = texture2D;
						break;
					}
				}
			}
			this.assertIsNotNull(grassTextureOrig, nameof(grassTextureOrig));

			this.assertIsFalse(string.IsNullOrEmpty(outputMeshRenderer.sharedMaterial.GetTag(replacementTag, false)),
				string.Format("outputMeshRenderer:{0}'s Material's Shader does not have replacementTag:\"{1}\"", outputMeshRenderer, replacementTag));
		}
#endif

		public void OnEnable() {
			init();
		}

		public void OnDisable() {
			reset();
		}

#if DEBUG_UGS
		public void OnDrawGizmosSelected() {
			var gc = Gizmos.color;
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireCube(areaGUI.center, areaGUI.size);
			Gizmos.color = gc;
		}
#endif

#endregion Unity callbacks
#region public

		public void renderArea(Bounds area) {
			var topMiddle = area.center;
			topMiddle.y += area.extents.y;
			setAreaForFuture(area);
			renderPosition(topMiddle);
		}

		public void setAreaForFuture(Bounds area) {
#if DEBUG_UGS
			areaGUI = area;
#endif
			snowCam.aspect = area.extents.x / area.extents.z;
			snowCam.orthographicSize = area.extents.z; // aspect works *from* screen height
			snowCam.farClipPlane = area.extents.y * 2f; // could have used size but only need y
			this.log($"snowCam area:{area} => extents:{area.extents} => aspect:{snowCam.aspect}, orthoSize:{snowCam.orthographicSize}, nearClipPlane:{snowCam.nearClipPlane}, farClipPlane:{snowCam.farClipPlane}");

			cameraRectNormalized = outputMeshXfrm.InverseTransformVector(area.size).toXZ() * meshLocalToNormalizedFactor;

			// TODO: Come back to why I was forcing these to ints -- was in OnValidate()
//			cameraRectNormalized.x = (int) cameraRectNormalized.x;
//			cameraRectNormalized.y = (int) cameraRectNormalized.y;

			this.log($"cameraRectNormalized:{cameraRectNormalized}");
		}

		public void renderPosition(Vector3 topMiddle) {
			this.assertIsFalse(float.IsNaN(cameraRectNormalized.x), "Call setAreaForFuture() before renderPosition()");

			camXfrm.position = topMiddle;

			// First convert camera center to mesh-coords, move it to the corner
			var camPosCornerNormalized = (outputMeshXfrm.InverseTransformPoint(topMiddle) - outputMeshLocalBounds.min).toXZ();
			camPosCornerNormalized *= meshLocalToNormalizedFactor; // then convert to normalized coordinates by dividing local position by size of the mesh (its local bounds)

			this.log($"camPosCornerNormalized:{camPosCornerNormalized} = topMiddle:{topMiddle} converted to local:{outputMeshXfrm.InverseTransformPoint(topMiddle)} with meshLocalBounds subtracted:{outputMeshLocalBounds.min} then multiplied by meshLocalToNormalizedFactor:{meshLocalToNormalizedFactor}");

			camPosCornerNormalized = convertNormalizedToUVNormalized(camPosCornerNormalized); // get in simple UV coords

			var rect = new Rect(camPosCornerNormalized - cameraRectNormalized / 2f, cameraRectNormalized);
			this.assertIsTrue(0f <= rect.xMin && rect.xMax <= 1f && 0f <= rect.yMin && rect.xMax <= 1f, rect);
			snowCam.rect = rect;

			// snowCam.targetTexture = rt; // ignores `rect` setting so...
			snowCam.SetTargetBuffers(rt.colorBuffer, rt.depthBuffer);
			Shader.SetGlobalTexture(IdGrassPresenceColorMapOrig, grassTextureOrig);
			snowCam.RenderWithShader(replacementShader, replacementTag);
			Shader.SetGlobalTexture(IdGrassPresenceColorMapOrig, null); // unset it?

			// Copy rt to mappedRT via mappingMaterial
			if (null == mappingMaterial) { // TODO: TEMP
				Graphics.Blit(rt, mappedRT); // Assumes no need of Linear/Gamma conversion
			} else {
				Graphics.Blit(rt, mappedRT, mappingMaterial); // Assumes no need of Linear/Gamma conversion
			}

#if UNITY_EDITOR
			if (!Application.isPlaying)
				snowCam.targetTexture = rt; // just to make preview easier
#endif
		}

#endregion public
#region internal
#endregion internal
#region private

		/// <summary>
		/// Done one time at first OnEnable() (or when used from Editor).
		/// </summary>
		private void init() {
			this.log("init");
			camXfrm = snowCam.transform;
			outputMeshXfrm = outputMeshRenderer.transform;
			// rt = snowCam.targetTexture;
			snowCam.enabled = false;
			Graphics.CopyTexture(grassTextureOrig, rt);
			Graphics.CopyTexture(grassTextureOrig, mappedRT);
			outputMeshRenderer.sharedMaterial.SetTexture(GrassPainterColorMapIdTemp, mappedRT);
#if UNITY_EDITOR // do default too, just in case using debug texture
			outputMeshRenderer.sharedMaterial.SetTexture(TextureIds[1], mappedRT);
#endif
			mappingMaterial = new Material(mappingMaterialPrefab); // instance it
			mappingMaterial.SetTexture(IdReverseMap, reverseMap);
			setupOutputMeshLocalBounds();
			snowCam.orthographicSize = 1f;
			snowCam.aspect = 1f;
//			this.log($"meshWorldBounds:{meshWorldBounds}, rtSize:{rtSize}, meshLocalToNormalizedFactor:{meshLocalToNormalizedFactor}");
		}

		private void reset() {
			this.log("reset");
			if (null != outputMeshRenderer && null != outputMeshRenderer.sharedMaterial) {
				outputMeshRenderer.sharedMaterial.SetTexture(GrassPainterColorMapIdTemp, grassTextureOrig);
			}
			if (null != mappingMaterial) {
				Destroy(mappingMaterial);
				mappingMaterial = null;
			}
			snowCam.enabled = true;
		}

		/// <summary>
		/// Convert from normalized coordinates to normalized UV-space coords using offset and factor which will invert if relevant.
		/// Normalized UV-space is used as input to <see cref="Camera.rect"/>.
		/// N.b. We /could/ use pixel coords here since conversionOffset and conversionFactor could be in pixels.
		/// </summary>
		private Vector2 convertNormalizedToUVNormalized(Vector2 v, bool silent = false) {
			this.assertIsTrue(0f <= v.x && v.x <= 1f && 0f <= v.y && v.y <= 1f, v);
			if (!silent) {
				this.log($"v:{v} => ???");
			}
			// TODO: Actual conversion v = (v + conversionOffset) * conversionFactor;
			this.assertIsTrue(0f <= v.x && v.x <= 1f && 0f <= v.y && v.y <= 1f, v);
			return v;
		}

#if UNITY_EDITOR
		/// <summary>
		/// Prepares <see cref="reverseMap"/> with mapping from mesh-normalized coords to texture.
		/// </summary>
		[ContextMenu("Setup reverse map")]
		private void setupReverseMap() {
			if (Application.isPlaying) {
				this.logWarn("Do not use at runtime -- only in Editor");
				return;
			}

			// Setup path and create image if none
			var create = null == reverseMap;
			string path;
            if (create) {
				path = UnityEditor.EditorUtility.SaveFilePanelInProject("Save reverse mapping texture", $"reverseMapping-{outputMeshRenderer.name}.png", "png", "Please save reverse mapping texture");
				if (string.IsNullOrEmpty(path)) {
					this.log("aborted saving reverse mapping texture");
                    return;
				}

				// Creation and saving of the reverse map Texture2D
				reverseMap = new Texture2D(grassTextureOrig.width, grassTextureOrig.height, grassTextureOrig.format, true);
			} else {
				if (!UnityEditor.EditorUtility.DisplayDialog("Modify reverseMap", string.Format("This will rewrite \"{0}\" with a new image.", reverseMap.name), "OK", "Cancel")) {
					this.log("Cancelled");
					return;
				}

				path = UnityEditor.AssetDatabase.GetAssetPath(reverseMap);
				var readableChanged = setTextureReadable(path, true); // temporarily make it readable
				this.assertIsTrue(readableChanged, "Setting \"{0}\" readable", path);
			}

			this.assertIsNotNull(reverseMap, "Null {0}", nameof(reverseMap));
			setupOutputMeshLocalBounds();
			var grassCollider = outputMeshRenderer.GetComponent<Collider>();
			this.assertIsNotNull(grassCollider);

			// Check MeshCollider and uses same mesh as renderer (required for correct UV outputs)
			if (!(grassCollider is MeshCollider)) {
				grassCollider.logError("Collider on {0} is not a MeshCollider = required", grassCollider);
				if (create) {
					setTextureReadable(path, false);
					return;
				}
			}
			var grassMeshCollider = (MeshCollider) grassCollider;
			var renderedMesh = outputMeshRenderer.GetComponent<MeshFilter>().sharedMesh;
            Mesh originalColliderMesh = null;
			if (grassMeshCollider.sharedMesh != renderedMesh) {
				this.logWarn("Temporarily setting MeshCollider's mesh to Renderer's mesh to get correct UV values.  Will assign back once done.");
				originalColliderMesh = grassMeshCollider.sharedMesh;
				grassMeshCollider.sharedMesh = renderedMesh;
			}

			// Setup values
			var corner = rotateReverseMap180 ? outputMeshLocalBounds.max : outputMeshLocalBounds.min; // aka offset
			corner.y = outputMeshLocalBounds.max.y + 1f; // to raycast from 1 above max height
			outputMeshXfrm = outputMeshRenderer.transform;
			var size = outputMeshLocalBounds.size;
			var rtSize = new Vector3Int(rt.width, 0, rt.height);
			// ReSharper disable twice RedundantCast -- ensure float output
			var factor = new Vector3(size.x / (float) rtSize.x, size.y, size.z / (float) rtSize.z); // size / rtSize
			var pixels = reverseMap.GetPixels32();
			var NoMapColour = new Color32(0, 0, 0, 0);
			const float maxDistance = float.MaxValue;
			const float DebugDuration = 4f;

			// Main loop casting rays and drawing image
			for (var z = 0; z < rtSize.z; z++) {
				for (var x = 0; x < rtSize.x; x++) {
					var pos = rotateReverseMap180 ? corner - new Vector3(x * factor.x, 0f, z * factor.z) :  corner + new Vector3(x * factor.x, 0f, z * factor.z);
					pos = outputMeshXfrm.TransformPoint(pos);
					var debugIt = (0 == z || z == rtSize.z - 1) && (0 == x || x == rtSize.x - 1);
					if (grassCollider.Raycast(new Ray(pos, Vector3.down), out var hitInfo, maxDistance)) {
						var coordV2 = hitInfo.textureCoord;
						//coordV2 = new Vector2(0f < coordV2.x ? 1f / coordV2.x : 0f, 0f < coordV2.y ? 1f / coordV2.y : 0f); // Invert?
						this.assertIsTrue(0f <= coordV2.x && coordV2.x <= 1f && 0f <= coordV2.y && coordV2.y <= 1f, coordV2);
						pixels[z * rtSize.x + x] = FixedColor32Helper.convert(coordV2);
						if (debugIt) {
							Debug.DrawLine(pos, hitInfo.point, new Color(0f, 1f, 0f, 0.5f), DebugDuration); // green
                            this.log($"x:{x}, z:{z} = pos:{pos} = coord:{coordV2} = {FixedColor32Helper.convert(coordV2)}");
						}
					} else {
						pixels[z * rtSize.x + x] = NoMapColour;
						if (debugIt) {
							Debug.DrawRay(pos, Vector3.down, new Color(1f, 0f, 0f, 0.5f), DebugDuration); // red
                            this.log($"x:{x}, z:{z} = pos:{pos} = no hit");
						}
					}
				}
			}

			// Restore MeshCollider if changed earlier (to get UV coords)
			if (null != originalColliderMesh) {
				grassMeshCollider.sharedMesh = originalColliderMesh; // TODO: A "restore" operation might be better (in case prefab)
			}

			// Save results
			reverseMap.SetPixels32(pixels);
			reverseMap.Apply(true, false);
			System.IO.File.WriteAllBytes(path, reverseMap.EncodeToPNG());
			reverseMap.log("Saved {0} for {1} as \"{2}\"", nameof(reverseMap), this, path);
			if (!create) {
				var readableChanged = setTextureReadable(path, false);
				this.assertIsTrue(readableChanged, "Setting \"{0}\" unreadable", path);
			}
			UnityEditor.AssetDatabase.Refresh();
			reverseMap = UnityEditor.AssetDatabase.LoadAssetAtPath<Texture2D>(path);
			reverseMap.log("{0} {1} setup with \"{2}\" as {3} assigned {4} pixels", this, nameof(reverseMap), path, reverseMap, pixels.Length);
		}

		private static bool setTextureReadable(string path, bool readable) {
			var importer = UnityEditor.AssetImporter.GetAtPath(path) as UnityEditor.TextureImporter;
			if (null == importer)
				return false;

			importer.isReadable = readable;

			// Couldn't get by build target
			// importer.ReadTextureImportInstructions(BuildTarget);

			// Couldn't get by deprecated API
			// var importSettings = new TextureImporterSettings();
			// importer.ReadTextureSettings(importSettings);
			// importSettings.for

			var defaultPlatformSettings = importer.GetDefaultPlatformTextureSettings();
			// TODO: Investigate why have to use RGBA32 to get smooth mapping data?
			// Used to use Automatic but losing detail somehow
			// defaultPlatformSettings.format = readable ? UnityEditor.TextureImporterFormat.RGBA32 : UnityEditor.TextureImporterFormat.Automatic;
			defaultPlatformSettings.format = UnityEditor.TextureImporterFormat.RGBA32;
			importer.SetPlatformTextureSettings(defaultPlatformSettings);

			importer.SaveAndReimport();
			return true;
		}

#endif // UNITY_EDITOR

		private void setupOutputMeshLocalBounds() {
			this.assertIsNotNull(outputMeshRenderer, "Null {0}", nameof(outputMeshRenderer));
			outputMeshLocalBounds = outputMeshRenderer.GetComponent<MeshFilter>().sharedMesh.bounds;
			meshLocalToNormalizedFactor = outputMeshLocalBounds.size.toXZInverted();
		}

#if DEBUG_UGS
		[ContextMenu("Render areaGUI")]
		private void renderAreaGUIDebugOnly() {
			debugSetup();
			renderArea(areaGUI);
		}

		[ContextMenu("Set areaGUI from actual pos (not bounds) and render")]
		private void setAreaGUIFromPosDebugOnly() {
			debugSetup();
			var position = camXfrm.position;
			areaGUI.center = position - new Vector3(0f, areaGUI.extents.y, 0f);
			renderPosition(position);
		}

		private void debugSetup() {
			camXfrm = snowCam.transform;
			if (!Application.isPlaying) {
				init();
			}

			if (float.IsNaN(cameraRectNormalized.x)) { // to ensure it's done once
				setAreaForFuture(areaGUI);
				this.log("area set from areaGUI");
			}
		}

		[ContextMenu("Reset output material")]
		private void resetOutputMaterial() {
			Graphics.CopyTexture(grassTextureOrig, rt);
			Graphics.CopyTexture(grassTextureOrig, mappedRT);
			outputMeshRenderer.sharedMaterial.SetTexture(GrassPainterColorMapIdTemp, grassTextureOrig);
		}
#endif

		private Transform camXfrm;

		/// <summary>
		/// Update area XZ in normalized space (<see cref="outputMeshRenderer"/> local space but normalized to its bounds).
		/// Suitable for use with <see cref="Camera.rect"/>.
		/// </summary>
		private Vector2 cameraRectNormalized = new Vector2(float.NaN, float.NaN);

		private Vector2 meshLocalToNormalizedFactor = new Vector2(float.NaN, float.NaN);

		private Bounds outputMeshLocalBounds;

		private Transform outputMeshXfrm;

		private Material mappingMaterial;

		private static readonly int IdGrassPresenceColorMapOrig = Shader.PropertyToID("_ColorMapOrig");

		private static readonly int GrassPainterColorMapIdTemp = Shader.PropertyToID("_ColorMap");

		private static readonly int[] TextureIds = { GrassPainterColorMapIdTemp, Shader.PropertyToID("_MainTex") };

		private static readonly int IdReverseMap = Shader.PropertyToID("_ReverseMap");

#endregion private
	}
}
