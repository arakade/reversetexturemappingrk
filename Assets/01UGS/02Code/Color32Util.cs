using UnityEngine;

namespace UGS.grass {
	public static class Color32Util {
		public static Color32 add(Color32 c1, Color32 c2) {
			return new Color32(
					clamp256byte(c1.r + c2.r),
					clamp256byte(c1.g + c2.g),
					clamp256byte(c1.b + c2.b),
					clamp256byte(c1.a + c2.a)
				);
		}

		public static Color32 add(float x, Color32 c) {
			return new Color32(
					clamp256byte(c.r + x),
					clamp256byte(c.g + x),
					clamp256byte(c.b + x),
					clamp256byte(c.a + x)
				);
		}

		public static Color32 add(Color32 c, float x) {
			return new Color32(
					clamp256byte(c.r + x),
					clamp256byte(c.g + x),
					clamp256byte(c.b + x),
					clamp256byte(c.a + x)
				);
		}

		public static Color32 sub(Color32 c1, Color32 c2) {
			return new Color32(
					clamp256byte(c1.r - c2.r),
					clamp256byte(c1.g - c2.g),
					clamp256byte(c1.b - c2.b),
					clamp256byte(c1.a - c2.a)
				);
		}

		public static Color32 sub(Color32 c, float x) {
			return new Color32(
					clamp256byte(c.r - x),
					clamp256byte(c.g - x),
					clamp256byte(c.b - x),
					clamp256byte(c.a - x)
				);
		}

		public static Color32 sub(float x, Color32 c) {
			return new Color32(
					clamp256byte(x - c.r),
					clamp256byte(x - c.g),
					clamp256byte(x - c.b),
					clamp256byte(x - c.a)
				);
		}

		public static Color32 mul(Color32 c, float x) {
			return new Color32(
					clamp256byte(c.r * x),
					clamp256byte(c.g * x),
					clamp256byte(c.b * x),
					clamp256byte(c.a * x)
				);
		}

		public static Color32 mul(this Color32 c, byte x) {
			return new Color32(
					clamp256byte(c.r * x),
					clamp256byte(c.g * x),
					clamp256byte(c.b * x),
					clamp256byte(c.a * x)
				);
		}

		public static byte clamp256byte(int x) {
			if (0 >= x) {
				return 0;
			}
			if (255 <= x) {
				return 255;
			}
			return (byte) x;
		}

		public static byte clamp256byte(float x) {
			if (0f >= x) {
				return 0;
			}
			if (255f <= x) {
				return 255;
			}
			return (byte) x;
		}
	}
}
