using System;
using UnityEngine;

namespace UGS.grass {
	public static class FixedColor32Helper {

		public const int Max = (1 << 16) - 1;

		private const byte ByteMax = Byte.MaxValue;

		public static Color32 convert(Vector2 v) {
			return convert(v.x, v.y);
		}

		public static Color32 convert(float x, float y) {
			if (0f > x || 1f < x) {
				throw new ArgumentOutOfRangeException(nameof(x), x, "x must be between 0 and 1 inclusive");
			}
			if (0f > y || 1f < y) {
				throw new ArgumentOutOfRangeException(nameof(y), y, "y must be between 0 and 1 inclusive");
			}
			int vx = (int) (x * Max);
			byte lx = (byte) vx;
			byte hx = (byte) (vx >> 8);

			int vy = (int) (y * Max);
			byte ly = (byte) vy;
			byte hy = (byte) (vy >> 8);

			return new Color32(hx, lx, hy, ly);
		}

		public static void convert(Color32 c, out float x, out float y) {
			int vx = c.r << 8 | c.g;
			int vy = c.b << 8 | c.a;
			x = vx / (float) Max;
			y = vy / (float) Max;
		}

		public static void convert(Color c, out float x, out float y) {
			int vx = (int) (c.r * ByteMax) << 8 | (int) (ByteMax * c.g);
			int vy = (int) (c.b * ByteMax) << 8 | (int) (ByteMax * c.a);
			x = vx / (float) Max;
			y = vy / (float) Max;
		}

	}
}
