using JetBrains.Annotations;
using UnityEngine;

namespace UGS.unityutils {
	public static class LogR {
		[System.Diagnostics.Conditional("DEBUG_UGS")] // from caller
		[StringFormatMethod("msgFmt")]
		public static void log(this UnityEngine.Object context, string msgFmt, params object[] args) {
			Debug.LogFormat(context, msgFmt, args);
		}

		[System.Diagnostics.Conditional("DEBUG_UGS")] // from caller
		[StringFormatMethod("msgFmt")]
		public static void logWarn(this UnityEngine.Object context, string msgFmt, params object[] args) {
			Debug.LogWarningFormat(context, msgFmt, args);
		}

		[System.Diagnostics.Conditional("DEBUG_UGS")] // from caller
		[StringFormatMethod("msgFmt")]
		public static void logError(this UnityEngine.Object context, string msgFmt, params object[] args) {
			Debug.LogErrorFormat(context, msgFmt, args);
		}

		[System.Diagnostics.Conditional("DEBUG_UGS")] // from caller
		public static void log(this UnityEngine.Object context, string msg) {
			Debug.Log(msg, context);
		}
	}
}
