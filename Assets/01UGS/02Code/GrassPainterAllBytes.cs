#if DIRECTX11_GRASS

using System;
using StixGames.GrassShader;
using UnityEngine;
using UnityEngine.Assertions;
using static UGS.grass.Color32Util;

namespace UGS.grass {
	[CreateAssetMenu(menuName = "Grass/Grass Painter AllBytes", fileName = "grassPainterAllBytes")]
	public class GrassPainterAllBytes : GrassPainterBase {

		public override void init() {
			//Get pixels
			pixels = CurrentTexture.GetPixels32();
			Assert.IsNotNull(pixels);
			Assert.IsTrue(0 < pixels.Length);
			Debug.LogFormat(this, "Got {0} pixels", pixels.Length);
		}

		public override void ApplyBrush(Vector3 hitPoint)
		{
			var currentTexture = CurrentTexture;
			if (currentTexture == null)
			{
				Debug.Log("You haven't created a texture for the current mode yet.");
				return;
			}

			RaycastHit hit;
			Vector2 texForward, texRight;
			if (!GrassManipulationUtility.GetWorldToTextureSpaceMatrix(new Ray(hitPoint + Vector3.up * 1000, Vector3.down),
				Epsilon, GrassColliders, out hit, out texForward, out texRight))
			{
				return;
			}

			Vector2 texCoord = hit.textureCoord;

			var fullSize = new Vector2Int(currentTexture.width, currentTexture.height);

			//Convert the world space radius to a pixel radius in texture space. This requires square textures.
			int pixelRadius = (int)(Size * texForward.magnitude * fullSize.x);

			//Calculate the pixel coordinates of the point where the raycast hit the texture.
			Vector2 mid = new Vector2(texCoord.x * fullSize.x, texCoord.y * fullSize.y);

			//Calculate the pixel area where the texture will be changed
			int targetStartX = (int)(mid.x - pixelRadius);
			int targetStartY = (int)(mid.y - pixelRadius);
			int startX = Mathf.Clamp(targetStartX, 0, fullSize.x);
			int startY = Mathf.Clamp(targetStartY, 0, fullSize.y);
			int width = Mathf.Min(targetStartX + pixelRadius * 2, fullSize.x) - targetStartX;
			int height = Mathf.Min(targetStartY + pixelRadius * 2, fullSize.y) - targetStartY;

			mid -= new Vector2(startX, startY);

//			Debug.LogFormat(this, $"texCoord:{texCoord}, pixelRadius:{pixelRadius}, mid:{mid}, targetStart:{targetStartX},{targetStartY}, start:{startX},{startY}, size:{width},{height}");
//			Debug.LogFormat(this,
//				$"start:{((new Vector2(0, 0) + mid) / pixelRadius) * 0.5f + new Vector2(0.5f, 0.5f)}" +
//				$"end:{((new Vector2(width - 1, height - 1) + mid) / pixelRadius) * 0.5f + new Vector2(0.5f, 0.5f)}");

			//Iterate trough all pixels
			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					int index = (y + startY) * fullSize.x + x + startX;
					Vector2 uv = ((new Vector2(x, y) - mid) / pixelRadius) * 0.5f + new Vector2(0.5f, 0.5f);
					try {
						pixels[index] = ApplyBrushToPixel(pixels[index], uv);
					} catch {
						Debug.LogErrorFormat(this, "index:{0}, pixels.Length:{1}", index, pixels.Length);
						throw;
					}
				}
			}

			//Save pixels and apply them to the texture
			currentTexture.SetPixels32(pixels);
			currentTexture.Apply();
		}

		private Color32 ApplyBrushToPixel(Color32 c, Vector2 v)
		{
			switch (Target)
			{
				case TextureTarget.ColorHeight:
					c = ColorHeightOperation(c, v);
					break;
				case TextureTarget.Density:
					c = DensityOperation(c, v);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			return c;
		}

		private Color32 ColorHeightOperation(Color32 c, Vector2 v)
		{
			Color32 newColor;
			switch (ColorHeightChannel)
			{
				case ColorHeightChannel.Color:
					newColor = BrushOperation(c, v);
					newColor.a = c.a;
					return newColor;
				case ColorHeightChannel.Height:
					newColor = BrushOperation(c, v);
					c.a = newColor.a;
					return c;
				case ColorHeightChannel.Both:
					return BrushOperation(c, v);
				default:
					throw new InvalidOperationException("Channel invalid");
			}
		}

		private Color32 DensityOperation(Color32 c, Vector2 v)
		{
			switch (DensityChannel)
			{
				case DensityChannel.R:
					c.r = BrushOperation(c.r, v);
					break;
				case DensityChannel.G:
					c.g = BrushOperation(c.g, v);
					break;
				case DensityChannel.B:
					c.b = BrushOperation(c.b, v);
					break;
				case DensityChannel.A:
					c.a = BrushOperation(c.a, v);
					break;
				default:
					throw new InvalidOperationException("Channel invalid");
			}
			return c;
		}

		private Color32 BrushOperation(Color32 x, Vector2 v)
		{
			v -= new Vector2(0.5f, 0.5f);
			v *= 2;
			var distance = v.magnitude; // TODO: sqrt() LOTS!

			//Calculate brush smoothness
			var value = SmoothStep(1, Mathf.Min(1 - Softness, 0.999f), distance);

			switch (Brush)
			{
				case BrushMode.Add:
					return add(x, mul(PaintColor32, value * Strength));

				case BrushMode.Subtract:
					return sub(x, mul(PaintColor32, value * Strength));

				case BrushMode.Blend:
					return add(mul(x, (1 - value * Strength)), mul(PaintColor32, Strength * value));

				default:
					throw new Exception("Unexpected " + Brush);
			}

//			return new Color32(
//				clamp256byte(result.r),
//				clamp256byte(result.g),
//				clamp256byte(result.b),
//				clamp256byte(result.a));
		}

		private byte BrushOperation(float x, Vector2 v)
		{
			v -= new Vector2(0.5f, 0.5f);
			v *= 2;
			var distance = v.magnitude;

			//Calculate brush smoothness
			var value = SmoothStep(1, Mathf.Min(1 - Softness, 0.999f), distance);

			float result;
			switch (Brush)
			{
				case BrushMode.Add:
					result = x + value * Strength;
					break;

				case BrushMode.Subtract:
					result = x - value * Strength;
					break;

				case BrushMode.Blend:
					result = x * (1 - value * Strength) + PaintDensity * Strength * value;
					break;

				default:
					throw new Exception("Unexpected " + Brush);
			}

			return clamp256byte(result);
			// return (byte) result; Maybe?
		}

		//Taken from wikipedia, smoothstep
		private float SmoothStep(float edge0, float edge1, float x)
		{
			// Scale, bias and saturate x to 0..1 range
			x = Mathf.Clamp01((x - edge0) / (edge1 - edge0));
			// Evaluate polynomial
			return x * x * (3 - 2 * x);
		}

		private Color32[] pixels;

	}
}

#endif // DIRECTX11_GRASS
