using UnityEngine;

namespace UGS.grass {
	public static class VectorUtils {
		public static Vector2 toXZ(this Vector3 v3) {
			return new Vector2(v3.x, v3.z);
		}

		public static Vector2 toXZInverted(this Vector3 v3) {
			return new Vector2(1f / v3.x, 1f / v3.z);
		}

		public static Vector3 fromXZ(this Vector2 v2, float y = 0f) {
			return new Vector3(v2.x, y, v2.y);
		}

	}
}
